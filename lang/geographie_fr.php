<?php

return [
	'configurer_communes_lier_objets_label' => 'Activer la liaison de communes sur les contenus',
	'configurer_titre' => 'Paramétrer les éléments géographiques',
];
