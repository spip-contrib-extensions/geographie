<?php

return [
	// A
	'ajouter_lien_geo_pays' => 'Ajouter ce pays',

	// C
	'confirmer_supprimer_geo_pays' => 'Confirmez-vous la suppression de ce pays ?',

	// I
	'icone_creer_geo_pays' => 'Créer un pays',
	'icone_modifier_geo_pays' => 'Modifier ce pays',
	'info_1_geo_pays' => 'Un pays',
	'info_aucun_geo_pays' => 'Aucun pays',
	'info_geo_pays_auteur' => 'Les pays de cet auteur',
	'info_nb_geo_pays' => '@nb@ pays',

	// R
	'retirer_lien_geo_pays' => 'Retirer ce pays',
	'retirer_tous_liens_geo_pays' => 'Retirer tous les pays',

	// S
	'supprimer_geo_pays' => 'Supprimer ce pays',

	// T
	'texte_ajouter_geo_pays' => 'Ajouter un pays',
	'texte_changer_statut_geo_pays' => 'Ce pays est :',
	'texte_creer_associer_geo_pays' => 'Créer et associer un pays',
	'titre_geo_pays' => '(Géo) Pays',
	'titre_geo_pays_rubrique' => 'Pays de la rubrique',
	'titre_logo_geo_pays' => 'Logo de ce pays',
];
