<?php

return [

	// T
	'titre_saisie_departement' => 'La liste des départements de France',
	'label_departement' => 'Département',
	'label_selectionnez' => 'Sélectionnez le département…',

];

