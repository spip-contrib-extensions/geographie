<?php

return [
	// A
	'ajouter_lien_geo_departement' => 'Ajouter ce département',

	// C
	'confirmer_supprimer_geo_departement' => 'Confirmez-vous la suppression de ce département ?',

	// I
	'icone_creer_geo_departement' => 'Créer un département',
	'icone_modifier_geo_departement' => 'Modifier ce département',
	'info_1_geo_departement' => 'Un département',
	'info_aucun_geo_departement' => 'Aucun département',
	'info_geo_departements_auteur' => 'Les départements de cet auteur',
	'info_nb_geo_departements' => '@nb@ départements',

	// R
	'retirer_lien_geo_departement' => 'Retirer ce département',
	'retirer_tous_liens_geo_departements' => 'Retirer tous les départements',

	// S
	'supprimer_geo_departement' => 'Supprimer ce département',

	// T
	'texte_ajouter_geo_departement' => 'Ajouter un département',
	'texte_changer_statut_geo_departement' => 'Ce département est :',
	'texte_creer_associer_geo_departement' => 'Créer et associer un département',
	'titre_geo_departement' => '(Géo) Département',
	'titre_geo_departements' => '(Géo) Départements',
	'titre_geo_departements_rubrique' => 'Départements de la rubrique',
	'titre_logo_geo_departement' => 'Logo de ce département',
];
